require 'yaml'
require 'rubygems'
require 'googl'
require 'twilip/version'

module Twilip
  class Client

    DEFAULT_CONFIG_FILE = "twilip.yml" 
    USER_AGENT = "Twilip - ruby api wrapper for microblogs - #{Twilip::VERSION}"
    
    def initialize(config_file_url = nil)
      @config_file = config_file_url ? config_file_url : DEFAULT_CONFIG_FILE
      
      load_adapters
    end
    
    def load_adapters
      lib_dir = File.dirname(__FILE__)
      full_pattern = File.join(lib_dir, 'adapters', '*.rb')
      Dir.glob(full_pattern).each { |file| require file }
    end
    
    def config
        YAML.load_file @config_file
    end
    
    def deliver(message, tags=nil)
      
      if tags
          @tags = tags.split(",").map {|m| m.strip }
      else
        @tags = []
      end
      
      preprocessed_message = preprocess_message(message)

      config["adapters"].each do |adapter|
        adapter_class = "#{adapter.capitalize}Adapter"
        adapter_class = Twilip.const_get(adapter_class)
        a_class = adapter_class.new config[adapter.to_s]
        a_class.deliver(preprocessed_message, @tags)
      end

    end

    def preprocess_message(message)
      # find urls :)
      url_regexp = /http[s]?:\/\/\w/
      url = message.split.grep(url_regexp).to_s
     
      short = Googl.shorten url
      message.gsub!(url, short.short_url)
    end

  end
end
