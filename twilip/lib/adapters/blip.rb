class BlipAdapter < BaseAdapter

URL = "http://api.blip.pl/updates"

  def deliver(message, tags = [])
    message = add_tags(message, tags)
    process_deliver(URL, {'body' => message[0,160]}, ';')
  end

end
