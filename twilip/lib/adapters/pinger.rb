require File.join(File.dirname(__FILE__),'base_adapter' )

class PingerAdapter < BaseAdapter
  URL = "http://a.pinger.pl/auth_add_message.json"

  def deliver(message, tags = [])
    message = add_tags(message, tags)
    process_deliver(URL, {"text" => message}, "&")
  end
  
end

