class FlakerAdapter < BaseAdapter
  URL = "http://api.flaker.pl/api/type:submit"

  def deliver(message, tags = [])

    message = add_tags(message, tags)

    process_deliver(URL, {"text" => message}, "&" )

  end

end
