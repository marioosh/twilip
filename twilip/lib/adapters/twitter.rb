require "rubygems"
require "twitter"

class TwitterAdapter < BaseAdapter
  
  def initialize(conf = {})
    
    Twitter.configure do |config|
      config.consumer_key = conf["consumer_key"]
      config.consumer_secret = conf["consumer_secret"]
      config.oauth_token = conf["oauth_token"]
      config.oauth_token_secret = conf["oauth_token_secret"]
    end

  end
  
  def deliver(message, tags = [])
    message = add_tags(message, tags)

   Twitter.update(message[0,140])

  end

end
