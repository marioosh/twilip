require 'net/http'
require 'uri'

class BaseAdapter
  attr_writer :headers
  def headers
    @headers ||= default_headers_map
  end

  def default_headers_map
    headers = []
    headers << ["User-Agent", Twilip::Client::USER_AGENT]
    headers << ["Accept", "application/json"]
    headers << ["Pragma", "no-cache"]
    headers
  end

  def add_headers(request)
    
    headers.each do |header|
      request.add_field(header[0], headers[1])
    end
    request
  end

  def initialize(config = {})
    @login = config["login"]
    @password = config["password"]
  end
  
  def add_tags(message, tags)
    @message = message.strip

    unless tags.empty?
      tags.each do |tag|
        @message << " ##{tag.strip}"
      end
    end
      @message
  end

  def process_deliver(api_url, *form_data)
    url = URI.parse(api_url)
    request = Net::HTTP::Post.new(url.path)
    request.basic_auth @login, @password
    request.set_form_data(*form_data)
    response = Net::HTTP.new(url.host, url.port).start {|http| http.request(add_headers(request)) }
    JSON.parse(response.body)
  end

end
