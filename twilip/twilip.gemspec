# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "twilip/version"

Gem::Specification.new do |s|
  s.name        = "twilip"
  s.version     = Twilip::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Mariusz Nosinski"]
  s.email       = ["marioosh@5dots.pl"]
  s.homepage    = "http://marioosh.5dots.pl"
  s.summary     = %q{Twilip is ruby wrapper for popular microblogs, like Twitter, Blip, Flaker and Pinger}
  s.description = %q{With twilip you can deliver one message to how many microblogs you want. All you need is another adapter and credentials in config file.}

  s.add_dependency('googl')
  s.add_dependency('twitter')

  # s.rubyforge_project = "twilip"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
