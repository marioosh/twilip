require "twilip"

describe Twilip do
  before do
    @message = "Nowy fajny message #{rand(1763871)} http://golababa.com"
    @tags = "tag1, tga2, tag3"
    @config_file = File.join(File.dirname(__FILE__), 'twilip.yml')
  end

  it "should be available from gem " do
    Twilip::VERSION.should be_true
  end

  it "should initialize client" do
    Twilip::Client.new.should be_true
  end

  it "should load configuration file" do
    @client = Twilip::Client.new @config_file
    @client.config.should_not be_empty
  end

  it "should deliver messages" do
   @client = Twilip::Client.new @config_file
   @client.deliver @message, @tags 
  end
end
